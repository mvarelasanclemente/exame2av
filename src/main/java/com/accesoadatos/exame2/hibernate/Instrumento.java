package com.accesoadatos.exame2.hibernate;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/* 
* TODO: engade as etiquetas necesarias para o funcionamento de Hibernate.
*/
public class Instrumento implements Serializable{
        
    private int id;
    private String nome;
    private Set<Musico> musicos;
    
    public Instrumento(){}
    
    public Instrumento(int id,String nome){
        this.id=id;
        this.nome=nome;
        this.musicos=new HashSet<>();
    }
    
    public void addMusico(Musico musico){
        this.musicos.add(musico);
    }
    
}
