package com.accesoadatos.exame2.hibernate;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/* 
* TODO: engade as etiquetas necesarias para o funcionamento de Hibernate.
*/
public class Musico implements Serializable{
    private int id;
    private String nome;
    private Set<Instrumento> instrumentos;   
    
    public Musico(){}
    
    public Musico(int id,String nome){
        this.id=id;
        this.nome=nome;
        this.instrumentos = new HashSet<>();
    }
    
    public void addInstrumento(Instrumento instrumento){
        this.instrumentos.add(instrumento);
    }
    
}
