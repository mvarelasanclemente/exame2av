package com.accesoadatos.exame2.hibernate;

import java.io.Serializable;

/* 
* TODO: engade as etiquetas necesarias para o funcionamento de Hibernate.
*/
public class CD implements Serializable{

    private int identificador;
    private String nome;
    private Grupo grupo;
    
    public CD(){}
    
    public CD(int id, String nome,Grupo grupo){
        this.identificador=id;
        this.nome=nome;
        this.grupo=grupo;
    }
    
    @Override
    public String toString(){
        return this.nome;
    }
    

}
