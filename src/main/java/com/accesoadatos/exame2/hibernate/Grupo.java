
package com.accesoadatos.exame2.hibernate;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/* 
* TODO: engade as etiquetas necesarias para o funcionamento de Hibernate.
*/
public class Grupo implements Serializable{

    private int identificador;
    private String nome;
    private Set<CD> cds;
    
    public Grupo(){}
    
    public Grupo(int id, String nome){
        this.identificador=id;
        this.nome=nome;
        this.cds = new HashSet<>();
    }
    
    public void addCD(CD cd){
        this.cds.add(cd);
    }
}
