package com.accesoadatos.exame2.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * NON TOCAR
 */
public class UtilSQLite {
    
    private static Connection conn;
    
    public static void connectDB(){
        String dbFile = "jdbc:sqlite:./exame2.db";
        createConnection(dbFile);
        
    }
    
    public static void closeDB(){
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(UtilSQLite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void createConnection(String url){
        try {
            conn=DriverManager.getConnection(url);
        } catch (SQLException ex) {
            Logger.getLogger(UtilSQLite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
   
    public static Connection getConnection(){
        return conn;
    }
    
    
    
    
}
