
package com.accesoadatos.exame2.sqlite;

import java.io.Serializable;

/*
NON TOCAR
*/ 
public class Mensaxe implements Serializable{
    
    private String asunto;
    private String corpo;
    
    public Mensaxe(){
        this.asunto = new String("");
        this.corpo= new String("");
    }
    
    public Mensaxe(String asunto,String corpo){
        this.asunto = asunto;
        this.corpo= corpo;
    }

    public String getAsunto() {
        return asunto;
    }

    public String getCorpo() {
        return corpo;
    }
    
    
    
}
