package com.accesoadatos.exame2.sqlite;

public class SQLTables {
    
    //Nome da táboa e atributos da tabla usuario
    public final static String tablaUsuario = "Usuario";
    public final static String usuarioUsername = "username";
    public final static String usuarioNome = "nome";

    //Nome da táboa e atributos da táboa mensaxes
    public final static String tablaMensaxe = "Mensaxe";
    public final static String mensaxeAsunto = "asunto";
    public final static String mensaxeCorpo = "corpo";
    public final static String mensaxeUsername = "username";
    
    //TODO: Crea a sentencia SQL que crea a táboa para gardar os usuarios
    private String sqlUsuario = 
            "";
    
    //TODO: Crea a sentencia SQL que crea a táboa para gardar as mensaxes
    private String sqlMensaxe = 
            "";
    
    
    //TODO: metodo que crea as táboas para os Usuarios e as mensaxes
    public void createDBs(){

    }
    
    //NON TOCAR
    public SQLTables(){}
    
}
