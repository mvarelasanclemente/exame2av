package com.accesoadatos.exame2.sqlite;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * NON TOCAR
 */
public class Usuario implements Serializable{
    private String nome;
    private String username;
    private ArrayList<Mensaxe> mensaxes;
    
    public Usuario(){
        this.nome = new String("");
        this.username = new String("");
        this.mensaxes = new ArrayList<>();
    }
    
    public Usuario(String nome, String username){
        this.nome = nome;
        this.username = username;
        this.mensaxes = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }

    public String getUsername() {
        return username;
    }

    public ArrayList<Mensaxe> getMensaxes() {
        return mensaxes;
    }
    
    public void addMensaxe(Mensaxe men){
        this.mensaxes.add(men);
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    
    
    
}
