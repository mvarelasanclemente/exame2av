package com.accesoadatos.exame2;

import com.accesoadatos.exame2.sqlite.ConsultaSQL;
import com.accesoadatos.exame2.sqlite.InsertSQL;
import com.accesoadatos.exame2.sqlite.Mensaxe;
import com.accesoadatos.exame2.sqlite.SQLTables;
import com.accesoadatos.exame2.sqlite.Usuario;
import com.accesoadatos.exame2.sqlite.UtilSQLite;
import com.accesoadatos.exame2.hibernate.CD;
import com.accesoadatos.exame2.hibernate.Consulta;
import com.accesoadatos.exame2.hibernate.Grupo;
import com.accesoadatos.exame2.hibernate.HibernateUtil;
import com.accesoadatos.exame2.hibernate.Instrumento;
import com.accesoadatos.exame2.hibernate.Musico;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Main {
    
    //TODO: Cubre cos teus datos
    public final static String apelidos = "";
    public final static String nome = "";
    public final static String DNI = "";
    
    //NON TOCAR A PARTIR DE AQUÍ AGÁS SE É NECEARIO PARA QUE COMPILE E SE EXECUTE O PROGRAMA
    public static Usuario user1,user2;
    
    public static void main(String[] args){
        
    
        //Conexión base de datos
        UtilSQLite.connectDB();
        
        /*************************************************
         * Exercicio 1: creación base de datos
         *************************************************/
        new SQLTables().createDBs();
        
        /*************************************************
         * Exercicio 2: engadir datos a a base de datos
         * ***********************************************/
        user1 = new Usuario("usuario1","username1");
        user1.addMensaxe(new Mensaxe("Mensaxe n1","Ola mundo 1!"));
        user1.addMensaxe(new Mensaxe("Mensaxe n2","Ola mundo 2!"));
        user1.addMensaxe(new Mensaxe("Mensaxe n3","Ola mundo 3!"));
        
        user2 = new Usuario("usuario2","username2");
        user2.addMensaxe(new Mensaxe("Mensaxe n4","Ola mundo 4!"));
        user2.addMensaxe(new Mensaxe("Mensaxe n5","Ola mundo 5!"));
        user2.addMensaxe(new Mensaxe("Mensaxe n6","Ola mundo 6!"));
        
        InsertSQL insql = new InsertSQL();
        insql.deleteAllData();
        insql.insertUsuarioEMensaxes(user1);
        insql.insertUsuarioEMensaxes(user2);
        

        /**************************************************
         * Exercicio 3: consulta SQLite
         * ************************************************/
        Usuario user = new ConsultaSQL().getUsuario(user2.getUsername());
        
        
        //Cerramos base de datos
        UtilSQLite.closeDB();
        

        
        try{
            /*****************************************************
             * Exercicio4: inicio consulta Hibernate
             ****************************************************/
            Session session = HibernateUtil.getSessionFactory(
                "192.168.56.101","3306","exame2",
                "userhibernate","abc123.","create-drop").openSession();

            /*****************************************************
             * Exercicio5: Engadir grupo e cd
             * Táboa "banda": atributos id e nomecompleto.
             * Táboa "disco": atributos id, titulo e idBanda
            ******************************************************/
            Grupo u2 = new Grupo(100, "U2");
            CD songsOfInoccence = new CD(2014, "Songs of Innocence", u2);
            CD noLineToHorizon = new CD(2009, "No Line to Horizon", u2);
            u2.addCD(songsOfInoccence);
            u2.addCD(noLineToHorizon);
            Grupo rolling = new Grupo(101, "Rolling Stones");
            CD undercorver = new CD(1983, "Undercover", rolling);
            CD aftermath = new CD(1966, "Alftermath", rolling);
            rolling.addCD(aftermath);
            rolling.addCD(undercorver);
            Transaction tran = session.beginTransaction();
            session.save(u2);
            session.save(rolling);
            tran.commit();
            
            
            /********************************************************
             * Exercicio6: Engadir Compositor e epoca
             * Táboa "instrumento" : atributos id e nome
             * Táboa "musico": atributos id e nome
             * Táboa "musIns: atributos idMusico e idInstrumento
            **********************************************************/
            Musico mozart = new Musico(1600,"Mozart");
            Musico beethoven = new Musico(1700,"Beethoven");
            Instrumento piano = new Instrumento(100,"Piano");
            Instrumento violin = new Instrumento(101,"Violin");
            Instrumento clave = new Instrumento(102,"Clave");
            mozart.addInstrumento(piano);
            mozart.addInstrumento(clave);
            mozart.addInstrumento(violin);
            beethoven.addInstrumento(piano);
            beethoven.addInstrumento(clave);
            piano.addMusico(mozart);
            piano.addMusico(beethoven);
            clave.addMusico(mozart);
            clave.addMusico(beethoven);
            violin.addMusico(mozart);
            
            Transaction tran2 = session.beginTransaction();
            session.save(mozart);
            session.save(beethoven);
            tran2.commit();
            
            /************************************************
             * Exercicio 7: Consulta os cds do grupo U2
             * **********************************************/
            List<CD> cds = new Consulta().cdsGrupo(session,"U2");
            for(CD cd:cds){
                System.out.println(cd.toString());
            }
  
        }catch(HibernateException e){
            e.printStackTrace();
        }
        
    }
    
}
